#!/bin/bash

echo

echo "--- emptying DB ..."
python3 manage.py flush --no-input

echo; echo

echo "--- Aplying migrations ..."
python3 manage.py migrate

echo; echo

echo "--- Test DB on production..."
python3 manage.py shell < Data/Create_DB.py

echo
