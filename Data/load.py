from django.contrib.auth.models import User
PASSWORD = 'admin'

# GETTERS


def get_user(username):
    return User.objects.get(username=username)


# loading users

def load_admin():
    admin = User(username='admin', email='admin@bla.com', is_superuser=True, is_staff=True)
    admin.set_password(PASSWORD)
    admin.save()
    print(
        " + Superuser '{}' added (this user is just for "
        "django-admin)".format(admin.username)
    )

def load_user(username, first_name, last_name, email):
    u = User(username=username, first_name=first_name, last_name=last_name, email=email)
    u.set_password(PASSWORD)
    u.save()
    print(" + Usuario '{}' agregado".format(u.username))

def load_users():
    load_admin()
    load_user('ivan', 'Ivan', 'Bolanos', 'ivan@bla.com')
    load_user('yamil', 'Yamil', 'Yambay', 'yamil@bla.com')
    load_user('rodrigo', 'Rodrigo', 'Ortellado', 'lennon@bla.com')
